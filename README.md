# Proyecto DesTapaIS

## Instalacion
Para iniciar el programa debes descargar el programa con git
```
git clone https://jorgelopezrosende@bitbucket.org/IS2-Grupo-4/destapais.git
```

Descarga la ultima version, luego hay que instalar las dependencias con python3.

```
pip install -r requiremets.txt
```

Para el correcto funcionamiento se debe inicializar los datos de las tablas. Ejecuta estos comandos para cargar los datos

Para cargar la administracion
```
python manage.py loaddata fixtures/initial_data.json
```

Para cargar el resto de datos de la aplicacion
```
python manage.py loaddata users
```

Luego ya puedes iniciar el servidor con: 

```
python manage.py runserver
```

## Urls
Una vez iniciado el server se puede acceder a estas ulrs

Aceso a la pagina principal [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

Acceso a la pagina de administracion [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)


