from django.urls import path, re_path, include

from . import views


urlpatterns = [
    path('signup/', views.SignUpView.as_view(), name='signup'),

    path('', include('django.contrib.auth.urls')),

    path('activate/<uidb64>/<token>/',
         views.ActivateView.as_view(), name='activate-account'),
]
