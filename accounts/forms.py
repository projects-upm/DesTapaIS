from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from django.http import HttpResponse
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.utils.html import strip_tags

from django_countries.fields import CountryField

from .models import *
from .tokens import account_activation_token
from users.models import Perfil


class SignupForm(UserCreationForm):
    email = forms.EmailField(max_length=200, help_text='Required', required=True, label='',
                             widget=forms.EmailInput(attrs={'placeholder': 'Email'}))

    nombre = forms.CharField(
        max_length=200, help_text='Required', required=True)

    apellidos = forms.CharField(
        max_length=200, help_text='Required', required=True)

    edad = forms.IntegerField(required=True)

    imagen = forms.ImageField(required=True)

    descripcion = forms.CharField(widget=forms.TextInput())

    pais = CountryField(blank=True, blank_label='(Select country)').formfield()

    def send_email(self, request):
        user = self.save(commit=False)
        user.is_active = False
        user.save()
        current_site = get_current_site(request)
        mail_subject = 'Activate your blog account.'
        message = render_to_string('accounts/acc_active_email.html', {
            'user': user,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
        })
        text_content = strip_tags(message)
        to_email = self.cleaned_data.get('email')
        email = EmailMultiAlternatives(
            'Activacion de cuenta', message, 'activarcuenta@destapais.es', [to_email])
        email.attach_alternative(text_content, "text/html")
        # email = EmailMessage(
        #     mail_subject, message, to=[to_email]
        # )
        email.send()

    def clean(self):
        cleaned_data = super().clean()
        edad = cleaned_data.get('edad')
        if edad < 18:
            self.add_error(
                'edad', 'Tienes que tener al menos 18 años para resgistrarte en DesTapaIS.')

        imagen = cleaned_data.get('imagen')
        if not imagen:
            data = 'images/img_usuario/None/no-img.jpg'

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    field_order = ('username', 'imagen', 'nombre', 'apellidos',
                   'edad', 'email', 'password1',
                   'password2', 'descripcion', 'pais')
