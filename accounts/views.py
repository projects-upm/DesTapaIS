from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic

from django.http import HttpResponse
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.core.mail import EmailMessage

from .forms import *
from users.models import User


class SignUpView(generic.CreateView):
    form_class = SignupForm
    success_url = reverse_lazy('login')
    template_name = 'accounts/signup.html'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        user = form.save()
        user.refresh_from_db()  # load the profile instance created by the signal
        user.perfil.nombre = form.cleaned_data.get('nombre')
        user.perfil.apellidos = form.cleaned_data.get('apellidos')
        user.perfil.edad = form.cleaned_data.get('edad')
        user.perfil.descripcion = form.cleaned_data.get('descripcion')
        user.perfil.pais = form.cleaned_data.get('pais')
        user.perfil.imagen = form.cleaned_data.get('imagen')
        user.save()
        form.send_email(self.request)
        return super().form_valid(form)


class ActivateView(generic.TemplateView):

    template_name = 'accounts/activate_account.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            uid = force_text(urlsafe_base64_decode(kwargs['uidb64']))
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and account_activation_token.check_token(user, kwargs['token']):
            user.is_active = True
            user.save()
            login(self.request, user)
            # return redirect('home')
            context['success'] = True
            return context
        else:
            context['success'] = False
            return context
