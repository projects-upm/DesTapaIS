
from django import forms

from django.utils.translation import gettext_lazy

from .models import *


class AmigosForm(forms.ModelForm):

    class Meta:
        model = Amigo
        exclude = []

    def clean(self):
        super(AmigosForm, self).clean()  # Thanks, @chefsmart
        de_perfil = self.cleaned_data.get('de_perfil', None)
        a_perfil = self.cleaned_data.get('a_perfil', None)
        if de_perfil == a_perfil:
            message = 'No pueden ser la misma id'
            raise forms.ValidationError(message)
        return self.cleaned_data


class PerfilForm(forms.ModelForm):
    imagen = forms.ImageField(required=True)

    class Meta:
        model = Perfil
        exclude = []


class DegustacionForm(forms.ModelForm):
    imagen = forms.ImageField(required=True)

    def clean(self):
        cleaned_data = super().clean()
        imagen = cleaned_data.get('imagen')
        if not imagen:
            data = 'images/img_degustacion/None/no-img.jpg'

    class Meta:
        model = Degustacion
        exclude = ['creador', 'calificaciones']


class LocalForm(forms.ModelForm):
    imagen = forms.ImageField(required=True)

    def clean(self):
        cleaned_data = super().clean()
        imagen = cleaned_data.get('imagen')
        if not imagen:
            data = 'images/img_local/None/no-img.jpg'

    class Meta:
        model = Local
        fields = ['nombre', 'pais', 'direccion', 'resenia', 'imagen']
        labels = {
            'resenia': gettext_lazy('Reseña'),
        }
