from django.contrib import admin
from django.db.models import Avg, Count

from .models import *
from .forms import *
# Register your models here.


@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'nombre', 'apellidos', 'imagen')


@admin.register(Amigo)
class AmigoAdmin(admin.ModelAdmin):
    form = AmigosForm

    list_display = ('id', 'de_perfil', 'a_perfil',
                    'estado', 'created', 'updated')


@admin.register(Local)
class LocalAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'direccion')


@admin.register(MeGusta)
class MeGustaAdmin(admin.ModelAdmin):
    list_display = ('id', 'perfil', 'local')


@admin.register(Degustacion)
class DegustacionAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'creador',
                    'num_calificaciones', 'calificacion_media')


@admin.register(Calificacion)
class CalificacionAdmin(admin.ModelAdmin):
    list_display = ('id', 'perfil', 'degustacion', 'voto')


@admin.register(Galardon)
class GalardonAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'icono')
    prepopulated_fields = {'slug': ('nombre',)}


@admin.register(GalardonConseguido)
class GalardonConseguidoAdmin(admin.ModelAdmin):
    list_display = ('id', 'perfil', 'galardon', 'nivel')


@admin.register(Actividad)
class ActividadAdmin(admin.ModelAdmin):
    list_display = ('id', 'perfil_id', 'accion', 'url')


@admin.register(Comentario)
class ActividadAdmin(admin.ModelAdmin):
    list_display = ('id', 'degustacion_id', 'perfil_id', 'comentario')
