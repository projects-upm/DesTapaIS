from django.urls import path, re_path, include

from . import views

from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', views.HomePageView.as_view(), name='homepage'),
    path('degustaciones/', include([
        path('', views.DegustacionesView.as_view(), name='degustaciones-list'),
        path('create/', login_required(views.DegustacionCreateView.as_view()),
             name='degustacion-create'),
        path('<int:pk>/details/',
             views.DegustacionDetailsView.as_view(), name='degustacion-details'),
        path('<int:pk>/calificar/<int:voto>/',
             login_required(views.calificar_degustacion), name='calificar-degustacion'),
        path('<int:pk>/comentario/', login_required(views.ComentarioCreateView.as_view()),
             name='degustacion-crear-comentario'),
    ])),
    path('perfil/', include([
        path('', login_required(views.PerfilView.as_view()), name='perfil-details'),
        path('editar/', login_required(views.UpdatePerfilView.as_view()),
             name='perfil-update'),
        path('degustaciones/', include([
            path('', login_required(views.PerfilDegustacionesView.as_view()),
                 name='perfil-degustaciones'),
            path('<int:pk>/editar/', login_required(views.DegustacionUpdateView.as_view()),
                 name='degustacion-update'),
            path('<int:pk>/borrar/', login_required(views.DegustacionDeleteView.as_view()),
                 name='degustacion-delete')
        ])),
        path('calificaciones/',
             login_required(views.PerfilCalificacionesView.as_view()),
             name='perfil-calificaciones'),
        path('amigos/', include([
            path('', login_required(views.AmigosView.as_view()),
                 name='perfil-amigos'),
            path('<int:pk>/borrar', login_required(views.borrar_amigo),
                 name='borrar-amigo'),
            path('<int:pk>/bloquear', login_required(views.bloquear_amigo),
                 name='bloquear-amigo'),
            path('<int:pk>/desbloquear', login_required(views.desbloquear_amigo),
                 name='desbloquear-amigo')
        ])),
        path('locales/', include([
            path('favoritos/', login_required(views.PerfilLocalesFavoritosView.as_view()),
                 name='perfil-locales'),
            path('<int:pk>/editar/',
                 login_required(views.LocalUpdateView.as_view()), name='local-update'),
            path('<int:pk>/borrar/',
                 login_required(views.LocalDeleteView.as_view()), name='local-delete')
        ])),
        path('galardones', include([
            path('', login_required(views.PerfilGalardonesView.as_view()),
                 name='perfil-galardones'),
        ])),
        path('actividades', include([
            path('', login_required(views.PerfilActividadView.as_view()),
                 name='perfil-actividades'),
            path('<int:pk>/vista', login_required(views.actividad_vista),
                 name='visitar-actividad')
        ])),
        path('peticiones/', include([
            path('recibidas/', login_required(views.PeticionesRecibidasView.as_view()),
                 name='peticiones-recibidas'),
            path('<int:pk>/aceptar', login_required(views.aceptar_peticion_amistad),
                 name='aceptar-peticion'),
            path('<int:pk>/denegar', login_required(views.denegar_peticion_amistad),
                 name='denegar-peticion'),
        ]))
    ])),
    path('locales/', include([
        path('', views.LocalesView.as_view(), name='locales-list'),
        path('<int:pk>/details/', views.LocalView.as_view(), name='local-details'),
        path('<int:pk>/crear-degustacion/', login_required(views.LocalCrearDegustacionView.as_view()),
             name='local-crear-degustacion'),
        path('create/', login_required(views.LocalCreateView.as_view()),
             name='local-create'),
        path('<int:pk>/add-me-gusta/',
             login_required(views.add_local_me_gusta), name='add-me-gusta-local'),
        path('<int:pk>/delete-me-gusta/',
             login_required(views.delete_local_me_gusta), name='delete-me-gusta-local')
    ])),
    path('usuarios/', include([
        path('', views.UsuariosView.as_view(), name='usuarios-list'),
        path('<int:pk>/', include([
            path('details/', views.UsuarioDetailsView.as_view(),
                 name='usuario-details'),
            path('solicitud/', views.enviar_solicitud_amistad,
                 name='enviar-solicitud'),
            path('degustaciones/', views.UsuarioDegustacionesView.as_view(),
                 name='usuario-degustaciones'),
            path('calificaciones/', views.UsuarioCalificacionesView.as_view(),
                 name='usuario-calificaciones'),
            path('actividades/', views.UsuarioActividadView.as_view(),
                 name='usuario-actividades'),
        ]))
    ])),
    path('error/', views.InformeDeErrorView.as_view(),
         name='informe-error'),
]
