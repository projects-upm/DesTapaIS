from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.contrib.auth.decorators import login_required
from django.urls import reverse, reverse_lazy
from django.core.paginator import Paginator
from django.views import generic
from django.http import Http404

from django.contrib.gis.geoip2 import GeoIP2

from .forms import *

# Create your views here.


class HomePageView(generic.TemplateView):
    template_name = 'homepage.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        search = self.request.GET.get('search', '')
        context['search'] = search

        context['degustaciones'] = Degustacion.objects.filter(nombre__contains=search).annotate(
            media=Avg('calificaciones_degustacion__voto')).order_by('-media', '-created')[:4]

        # TODO Cambiar la ip
        g = GeoIP2()
        ip = self.request.META.get('REMOTE_ADDR', None)
        if ip:
            pais = g.country_code('80.58.61.250')
        else:
            pais = 'ES'

        context['locales'] = Local.objects.filter(
            pais=pais, nombre__contains=search).order_by('nombre', '-created')[:4]

        context['usuarios'] = Perfil.objects.filter(
            Q(nombre__contains=search) |
            Q(apellidos__contains=search) |
            Q(user__email__contains=search)
        ).annotate(
            media=Avg('creador__calificaciones_degustacion__voto')).order_by('-media', '-created')[:4]

        return context


class DegustacionesView(generic.ListView):
    model = Degustacion
    template_name = 'degustaciones/degustaciones-list.html'

    context_object_name = 'latest_degustaciones_list'

    def get_queryset(self):
        degustaciones = super().get_queryset()

        search = self.request.GET.get('search', '')
        order = self.request.GET.get('order', '-media')
        page = self.request.GET.get('page', 1)
        show_all = self.request.GET.get('all', False)

        tipo = self.request.GET.get('tipo', None)
        tamanio = self.request.GET.get('tamanio', None)
        gusto = self.request.GET.get('gusto', None)

        if tipo:
            degustaciones = degustaciones.filter(tipo=tipo)

        if tamanio:
            degustaciones = degustaciones.filter(tamanio=tamanio)

        if gusto:
            degustaciones = degustaciones.filter(gusto=gusto)

        degustaciones = degustaciones.filter(nombre__contains=search).annotate(
            media=Avg('calificaciones_degustacion__voto')).order_by(order, '-created')

        if not show_all:
            """Devuelve las ultimas 10 degustaciones mas valoradas."""
            paginator = Paginator(degustaciones, 10)
            return paginator.get_page(page)
        else:
            return degustaciones

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('search', '')
        context['order'] = self.request.GET.get('order', '-media')
        context['all'] = self.request.GET.get('all', False)

        if self.request.GET.get('tipo', None):
            context['tipo'] = TIPOS[int(self.request.GET.get('tipo', None))][1]
        else:
            context['tipo'] = None

        if self.request.GET.get('tamanio', None):
            context['tamanio'] = TAMANIOS[int(
                self.request.GET.get('tamanio', None))][1]
        else:
            context['tamanio'] = None

        if self.request.GET.get('gusto', None):
            context['gusto'] = GUSTOS[int(
                self.request.GET.get('gusto', None))][1]
        else:
            context['gusto'] = None

        return context


class DegustacionDetailsView(generic.DetailView):
    model = Degustacion
    template_name = 'degustaciones/degustacion-details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_authenticated:
            perfil = self.request.user.perfil

            degustacion = self.get_object()

            perfil.actividades.create(accion='Has visto la desgustacion {}.'.format(
                degustacion.nombre), url=degustacion.get_absolute_url(), vista=True)

            if perfil in degustacion.calificaciones.all():
                context['perfil_voto'] = perfil.calificaciones_perfil.get(
                    degustacion=self.get_object().pk).voto

        return context


class DegustacionCreateView(generic.CreateView):
    model = Degustacion
    template_name = 'degustaciones/degustacion-create.html'
    fields = ['local', 'nombre', 'descripcion',
              'imagen', 'tipo', 'tamanio', 'gusto']

    def form_valid(self, form):
        form.instance.creador = self.request.user.perfil
        super(DegustacionCreateView, self).form_valid(form)

        perfil = self.request.user.perfil

        perfil.actividades.create(accion='Has creado la degustacion {}.'.format(
            self.object.nombre), url=self.object.get_absolute_url())

        galardon = Galardon.objects.get(slug='foodie')
        galardon_self = perfil.galardones_conseguidos.filter(galardon=galardon)

        if perfil.creador.count() >= 160 and galardon_self.nivel == 4:
            galardon_self.update(nivel=5)
        elif perfil.creador.count() >= 80 and galardon_self.nivel == 3:
            galardon_self.update(nivel=4)
        elif perfil.creador.count() >= 40 and galardon_self.nivel == 2:
            galardon_self.update(nivel=3)
        elif perfil.creador.count() >= 20 and galardon_self.nivel == 1:
            galardon_self.update(nivel=2)
        elif perfil.creador.count() >= 10 and not galardon_self:
            perfil.galardones_conseguidos.create(galardon=galardon, nivel=1)

        return redirect('degustaciones-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['previous_url'] = self.request.META.get('HTTP_REFERER')
        return context


class DegustacionUpdateView(generic.UpdateView):
    model = Degustacion
    template_name = 'degustaciones/degustacion-update.html'
    fields = ['nombre', 'descripcion', 'imagen', 'tipo', 'tamanio', 'gusto']

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.creador != self.request.user.perfil:
            return redirect('degustaciones-list')
        return super(DegustacionUpdateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['previous_url'] = self.request.META.get('HTTP_REFERER')
        return context


class DegustacionDeleteView(generic.DeleteView):
    model = Degustacion
    template_name = 'degustaciones/degustacion-confirm-delete.html'
    success_url = reverse_lazy('degustaciones-list')

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.creador != self.request.user.perfil:
            return redirect('degustaciones-list')
        return super(DegustacionDeleteView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['previous_url'] = self.request.META.get('HTTP_REFERER')
        return context


def calificar_degustacion(request, pk, voto):
    perfil = request.user.perfil
    degustacion = Degustacion.objects.get(pk=pk)
    perfil.calificar_degustacion(degustacion, voto)
    return redirect('degustacion-details', pk=pk)


class PerfilDegustacionesView(generic.ListView):
    model = Degustacion
    template_name = 'perfil/degustaciones-perfil.html'

    context_object_name = 'degustaciones_perfil_list'

    def get_queryset(self):
        """Devuelve las degustaciones creadas por el usuario y mas valoradas"""
        degustaciones = self.request.user.perfil.creador.all()

        search = self.request.GET.get('search', '')
        order = self.request.GET.get('order', '-media')
        page = self.request.GET.get('page', 1)
        show_all = self.request.GET.get('all', False)

        tipo = self.request.GET.get('tipo', None)
        tamanio = self.request.GET.get('tamanio', None)
        gusto = self.request.GET.get('gusto', None)

        if tipo:
            degustaciones = degustaciones.filter(tipo=tipo)

        if tamanio:
            degustaciones = degustaciones.filter(tamanio=tamanio)

        if gusto:
            degustaciones = degustaciones.filter(gusto=gusto)

        degustaciones = degustaciones.filter(nombre__contains=search).annotate(
            media=Avg('calificaciones_degustacion__voto')).order_by(order, '-created')

        if not show_all:
            """Devuelve las ultimas 10 degustaciones mas valoradas."""
            paginator = Paginator(degustaciones, 10)
            return paginator.get_page(page)
        else:
            return degustaciones

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('search', '')
        context['order'] = self.request.GET.get('order', '-media')
        context['all'] = self.request.GET.get('all', False)

        if self.request.GET.get('tipo', None):
            context['tipo'] = TIPOS[int(self.request.GET.get('tipo', None))][1]
        else:
            context['tipo'] = None

        if self.request.GET.get('tamanio', None):
            context['tamanio'] = TAMANIOS[int(
                self.request.GET.get('tamanio', None))][1]
        else:
            context['tamanio'] = None

        if self.request.GET.get('gusto', None):
            context['gusto'] = GUSTOS[int(
                self.request.GET.get('gusto', None))][1]
        else:
            context['gusto'] = None

        return context


class PerfilCalificacionesView(generic.ListView):
    model = Calificacion
    template_name = 'perfil/calificaciones-perfil.html'

    context_object_name = 'calificaciones_perfil_list'

    def get_queryset(self):
        """Devuelve las degustaciones creadas por el usuario y mas valoradas"""
        return self.request.user.perfil.calificaciones_perfil.all().order_by('-voto', '-created')


class PerfilLocalesFavoritosView(generic.ListView):
    model = Local
    template_name = 'perfil/locales-favoritos.html'

    context_object_name = 'locales_favoritos_perfil_list'

    def get_queryset(self):
        locales = self.request.user.perfil.me_gustas.all()

        search = self.request.GET.get('search', '')
        order = self.request.GET.get('order', 'nombre')
        page = self.request.GET.get('page', 1)
        show_all = self.request.GET.get('all', False)

        locales = locales.filter(
            nombre__contains=search).order_by(order, '-created')

        if not show_all:
            """Devuelve las ultimas 10 degustaciones mas valoradas."""
            paginator = Paginator(locales, 10)
            return paginator.get_page(page)
        else:
            return locales

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('search', '')
        context['order'] = self.request.GET.get('order', 'nombre')
        context['all'] = self.request.GET.get('all', False)
        context['locales_mapa'] = Local.objects.all()

        return context


class PerfilActividadView(generic.ListView):
    model = Actividad
    template_name = 'perfil/actividad-perfil.html'

    context_object_name = 'actividades_perfil_list'

    def get_queryset(self):
        return self.request.user.perfil.actividades.all()


def actividad_vista(request, pk):
    actividad = Actividad.objects.get(pk=pk)
    actividad.vista = True
    actividad.save()
    return redirect(actividad.url)


class PerfilGalardonesView(generic.ListView):
    model = Galardon
    template_name = 'perfil/galardones-perfil.html'

    context_object_name = 'galardones_list'


class PerfilView(generic.DetailView):
    model = Perfil
    template_name = 'perfil/perfil-details.html'

    def get_object(self, queryset=None):
        return self.request.user.perfil


class UpdatePerfilView(generic.UpdateView):
    model = Perfil
    template_name = 'perfil/perfil-update.html'
    fields = ['nombre', 'apellidos', 'edad', 'imagen', 'descripcion']

    def get_object(self, queryset=None):
        return self.request.user.perfil

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['previous_url'] = self.request.META.get('HTTP_REFERER')
        return context


class PeticionesRecibidasView(generic.TemplateView):
    template_name = 'perfil/peticiones-recibidas.html'


class AmigosView(generic.ListView):
    model = Perfil
    template_name = 'perfil/amigos-perfil.html'

    context_object_name = 'amigos_list'

    def get_queryset(self):
        perfil = self.request.user.perfil

        search = self.request.GET.get('search', '')
        order = self.request.GET.get('order', 'nombre')

        amigos = perfil.lista_de_amigos().filter(
            nombre__contains=search).order_by(order, '-created')

        return amigos

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('search', '')
        context['order'] = self.request.GET.get('order', '')
        return context


def enviar_peticion_amistad(request, pk):
    perfil = request.user.perfil
    pk = Perfil.objects.get(pk=pk)
    perfil.aniadir_amigo(perfil=pk, estado=0)
    return redirect('perfil-amigos')


def aceptar_peticion_amistad(request, pk):
    perfil = request.user.perfil
    pk = Perfil.objects.get(pk=pk)
    perfil.aceptar_peticion(perfil=pk)
    return redirect('perfil-amigos')


def denegar_peticion_amistad(request, pk):
    perfil = request.user.perfil
    pk = Perfil.objects.get(pk=pk)
    perfil.denegar_peticion(perfil=pk)
    return redirect('perfil-amigos')


def borrar_amigo(request, pk):
    perfil = request.user.perfil
    pk = Perfil.objects.get(pk=pk)
    perfil.borrar_amigo(pk)
    return redirect('perfil-amigos')


def bloquear_amigo(request, pk):
    perfil = request.user.perfil
    pk = Perfil.objects.get(pk=pk)
    perfil.bloquear_amigo(pk)
    return redirect('perfil-amigos')


def desbloquear_amigo(request, pk):
    perfil = request.user.perfil
    pk = Perfil.objects.get(pk=pk)
    perfil.desbloquear_amigo(pk)
    return redirect('perfil-amigos')


def handler404(request, exception):
    template = 'errors/404.html'
    if request.path.startswith('/degustaciones'):
        template = 'errors/degustaciones-404.html'
    elif True:
        pass
    return render(request, template_name=template, status=404)


class LocalesView(generic.ListView):
    model = Local
    template_name = 'locales/locales-list.html'
    context_object_name = 'locales_list'

    def get_queryset(self):
        locales = super().get_queryset()

        # TODO Cambiar la ip
        g = GeoIP2()
        ip = self.request.META.get('REMOTE_ADDR', None)
        if ip:
            pais = g.country_code('80.58.61.250')
        else:
            pais = 'ES'

        search = self.request.GET.get('search', '')
        order = self.request.GET.get('order', 'nombre')
        page = self.request.GET.get('page', 1)
        show_all = self.request.GET.get('all', False)

        locales = locales.filter(
            pais=pais, nombre__contains=search).order_by(order, '-created')

        if not show_all:
            """Devuelve las ultimas 10 degustaciones mas valoradas."""
            paginator = Paginator(locales, 10)
            return paginator.get_page(page)
        else:
            return locales

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('search', '')
        context['order'] = self.request.GET.get('order', 'nombre')
        context['all'] = self.request.GET.get('all', False)
        context['locales_mapa'] = Local.objects.all()

        # TODO Cambiar la ip
        g = GeoIP2()
        ip = self.request.META.get('REMOTE_ADDR', None)
        if ip:
            pais = g.country_code('80.58.61.250')
        else:
            pais = 'ES'

        context['pais'] = pais

        return context


class LocalView(generic.DetailView):
    model = Local
    template_name = 'locales/local-details.html'

    def get_object(self, queryset=None):
        local = super().get_object()
        if self.request.user:
            self.request.user.perfil.actividades.create(accion='Has visto el local {}.'.format(
                local.nombre), url=local.get_absolute_url(), vista=True)
        return local


class LocalCrearDegustacionView(generic.CreateView):
    model = Degustacion
    template_name = 'degustaciones/degustacion-create.html'
    fields = ['nombre', 'descripcion', 'imagen', 'tipo', 'tamanio', 'gusto']

    def form_valid(self, form):
        form.instance.creador = self.request.user.perfil
        form.instance.local = Local.objects.get(pk=self.kwargs['pk'])
        super(LocalCrearDegustacionView, self).form_valid(form)

        perfil = self.request.user.perfil

        perfil.actividades.create(accion='Has creado la degustacion {}.'.format(
            self.object.nombre), url=self.object.get_absolute_url())

        galardon = Galardon.objects.get(slug='foodie')
        galardon_self = perfil.galardones_conseguidos.filter(galardon=galardon)

        if perfil.creador.count() >= 160 and galardon_self.nivel == 4:
            galardon_self.update(nivel=5)
        elif perfil.amigos.count() >= 80 and galardon_self.nivel == 3:
            galardon_self.update(nivel=4)
        elif perfil.amigos.count() >= 40 and galardon_self.nivel == 2:
            galardon_self.update(nivel=3)
        elif perfil.amigos.count() >= 20 and galardon_self.nivel == 1:
            galardon_self.update(nivel=2)
        elif perfil.amigos.count() >= 10 and not galardon_self:
            perfil.galardones_conseguidos.create(galardon=galardon, nivel=1)

        return redirect('local-details', pk=str(self.kwargs['pk']))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['previous_url'] = self.request.META.get('HTTP_REFERER')
        return context


class LocalCreateView(generic.CreateView):
    form_class = LocalForm
    model = Local
    template_name = 'locales/local-create.html'
    # fields = ['nombre', 'pais', 'direccion', 'resenia', 'imagen']

    def form_valid(self, form):
        form.instance.creador = self.request.user.perfil
        super(LocalCreateView, self).form_valid(form)

        perfil = self.request.user.perfil

        perfil.actividades.create(accion='Has creado el local {}.'.format(
            self.object.nombre), url=self.object.get_absolute_url())

        galardon = Galardon.objects.get(slug='explorador')
        galardon_self = perfil.galardones_conseguidos.filter(galardon=galardon)

        if perfil.creador_local.count() >= 48 and galardon_self.nivel == 4:
            galardon_self.update(nivel=5)
        elif perfil.creador_local.count() >= 24 and galardon_self.nivel == 3:
            galardon_self.update(nivel=4)
        elif perfil.creador_local.count() >= 12 and galardon_self.nivel == 2:
            galardon_self.update(nivel=3)
        elif perfil.creador_local.count() >= 6 and galardon_self.nivel == 1:
            galardon_self.update(nivel=2)
        elif perfil.creador_local.count() >= 3 and not galardon_self:
            perfil.galardones_conseguidos.create(galardon=galardon, nivel=1)

        return redirect('locales-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['previous_url'] = self.request.META.get('HTTP_REFERER')
        return context


class LocalUpdateView(generic.UpdateView):
    model = Local
    template_name = 'locales/local-update.html'
    fields = ['nombre', 'pais', 'direccion', 'resenia', 'imagen']

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.creador != self.request.user.perfil:
            return redirect('locales-list')
        return super(LocalUpdateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['previous_url'] = self.request.META.get('HTTP_REFERER')
        return context


class LocalDeleteView(generic.DeleteView):
    model = Local
    template_name = 'locales/local-delete.html'
    success_url = reverse_lazy('locales-list')

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.creador != self.request.user.perfil:
            return redirect('locales-list')
        return super(LocalDeleteView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['previous_url'] = self.request.META.get('HTTP_REFERER')
        return context


def add_local_me_gusta(request, pk):
    perfil = request.user.perfil
    local = Local.objects.get(pk=pk)
    perfil.add_me_gusta_local(local)
    return redirect(request.META.get('HTTP_REFERER'))


def delete_local_me_gusta(request, pk):
    perfil = request.user.perfil
    local = Local.objects.get(pk=pk)
    perfil.delete_me_gusta_local(local)
    return redirect(request.META.get('HTTP_REFERER'))


class UsuariosView(generic.ListView):
    model = Perfil
    template_name = 'users/usuarios-list.html'
    context_object_name = 'usuarios_list'

    def get_queryset(self):
        usuarios = super().get_queryset()

        usuarios = usuarios.exclude(user__is_superuser=True)

        search = self.request.GET.get('search', '')
        order = self.request.GET.get('order', '-media')
        page = self.request.GET.get('page', 1)
        show_all = self.request.GET.get('all', False)

        if order == 'media' or order == '-media':
            usuarios = usuarios.annotate(
                media=Avg('creador__calificaciones_degustacion__voto')).order_by(order, '-created')

        elif order == 'num' or order == '-num':
            usuarios = usuarios.annotate(
                num=Count('creador')).order_by(order, '-created')
        else:
            usuarios = usuarios.order_by(order, '-created')

        usuarios = usuarios.filter(
            Q(nombre__contains=search) | Q(user__email__contains=search))

        if not show_all:
            """Devuelve las ultimas 10 degustaciones mas valoradas."""
            paginator = Paginator(usuarios, 10)
            return paginator.get_page(page)
        else:
            return usuarios

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('search', '')
        context['order'] = self.request.GET.get('order', '-media')
        context['all'] = self.request.GET.get('all', False)

        return context


class UsuarioDetailsView(generic.DetailView):
    model = Perfil
    template_name = 'users/usuario-details.html'

    def get_object(self, queryset=None):
        profile = super().get_object()

        if self.request.user.is_authenticated:
            perfil = self.request.user.perfil

            perfil.actividades.create(accion='Has visitado el perfil de {}.'.format(
                profile.nombre), url=profile.get_absolute_url(), vista=True)

        if not profile.user.is_superuser:
            return profile
        else:
            raise Http404


class UsuarioDegustacionesView(generic.ListView):
    model = Degustacion
    template_name = 'users/usuario-degustaciones.html'

    context_object_name = 'degustaciones_usuario_list'

    def get_queryset(self):

        profile = Perfil.objects.get(pk=self.kwargs['pk'])
        if profile.user.is_superuser:
            raise Http404

        degustaciones = profile.creador.all()

        search = self.request.GET.get('search', '')
        order = self.request.GET.get('order', '-media')
        page = self.request.GET.get('page', 1)
        show_all = self.request.GET.get('all', False)

        tipo = self.request.GET.get('tipo', None)
        tamanio = self.request.GET.get('tamanio', None)
        gusto = self.request.GET.get('gusto', None)

        if tipo:
            degustaciones = degustaciones.filter(tipo=tipo)

        if tamanio:
            degustaciones = degustaciones.filter(tamanio=tamanio)

        if gusto:
            degustaciones = degustaciones.filter(gusto=gusto)

        degustaciones = degustaciones.filter(nombre__contains=search).annotate(
            media=Avg('calificaciones_degustacion__voto')).order_by(order, '-created')

        if not show_all:
            """Devuelve las ultimas 10 degustaciones mas valoradas."""
            paginator = Paginator(degustaciones, 10)
            return paginator.get_page(page)
        else:
            return degustaciones

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['perfil'] = Perfil.objects.get(pk=self.kwargs['pk'])
        context['search'] = self.request.GET.get('search', '')
        context['order'] = self.request.GET.get('order', '-media')
        context['all'] = self.request.GET.get('all', False)

        if self.request.GET.get('tipo', None):
            context['tipo'] = TIPOS[int(self.request.GET.get('tipo', None))][1]
        else:
            context['tipo'] = None

        if self.request.GET.get('tamanio', None):
            context['tamanio'] = TAMANIOS[int(
                self.request.GET.get('tamanio', None))][1]
        else:
            context['tamanio'] = None

        if self.request.GET.get('gusto', None):
            context['gusto'] = GUSTOS[int(
                self.request.GET.get('gusto', None))][1]
        else:
            context['gusto'] = None

        return context


class UsuarioCalificacionesView(generic.ListView):
    model = Calificacion
    template_name = 'users/usuario-calificaciones.html'

    context_object_name = 'calificaciones_usuario_list'

    def get_queryset(self):

        profile = Perfil.objects.get(pk=self.kwargs['pk'])
        if profile.user.is_superuser:
            raise Http404

        calificaciones = profile.calificaciones_perfil.all()

        calificaciones = calificaciones.order_by('-voto', '-created')

        return calificaciones

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['perfil'] = Perfil.objects.get(pk=self.kwargs['pk'])
        return context


def enviar_solicitud_amistad(request, pk):
    perfil = request.user.perfil
    amigo = Perfil.objects.get(pk=pk)
    perfil.aniadir_amigo(amigo)
    return redirect(request.META.get('HTTP_REFERER'))


class UsuarioActividadView(generic.ListView):
    model = Actividad
    template_name = 'users/usuario-actividad.html'

    context_object_name = 'actividades_usuario_list'

    def get_queryset(self):
        profile = Perfil.objects.get(pk=self.kwargs['pk'])
        if profile.user.is_superuser:
            raise Http404

        actividades = profile.actividades.all()

        actividades = actividades.order_by('-created')
        return actividades

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['perfil'] = Perfil.objects.get(pk=self.kwargs['pk'])
        return context


class ComentarioCreateView(generic.CreateView):
    model = Comentario
    template_name = 'degustaciones/degustacion-crear-comentario.html'
    fields = ['comentario']

    def form_valid(self, form):
        form.instance.perfil = self.request.user.perfil
        form.instance.degustacion = Degustacion.objects.get(
            pk=self.kwargs['pk'])
        super(ComentarioCreateView, self).form_valid(form)

        perfil = self.request.user.perfil
        degustacion = Degustacion.objects.get(
            pk=self.kwargs['pk'])

        perfil.actividades.create(accion='Has comentado la degustacion {}.'.format(
            degustacion.nombre), url=degustacion.get_absolute_url(), vista=True)

        return redirect('degustacion-details', pk=str(self.kwargs['pk']))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['previous_url'] = self.request.META.get('HTTP_REFERER')
        return context


class InformeDeErrorView(generic.TemplateView):
    template_name = 'informe-error.html'
