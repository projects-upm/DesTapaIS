from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models import Avg, Count
from django.urls import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q

from django_countries.fields import CountryField
# Create your models here.

ESTADO_PENDIENTE = 1
ESTADO_ACEPTADA = 2
ESTADO_DENEGADA = 3
ESTADO_BLOQUEADA = 4
ESTADO_AMISTAD = [
    (ESTADO_PENDIENTE, 'Pendiente'),
    (ESTADO_ACEPTADA, 'Aceptada'),
    (ESTADO_DENEGADA, 'Denegada'),
    (ESTADO_BLOQUEADA, 'Bloqueada'),
]


class Perfil(models.Model):
    '''Perfil del Usuario'''
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name='perfil'
    )
    nombre = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200, null=True, blank=True)
    edad = models.PositiveIntegerField(
        validators=[MinValueValidator(18)], null=True)
    imagen = models.ImageField(
        upload_to='images/img_usuario/', default='images/img_usuario/None/no-img.jpg')
    descripcion = models.TextField(null=True, blank=True)
    pais = CountryField(null=True, blank=True)
    amigos = models.ManyToManyField(
        'self', symmetrical=False, through='Amigo', related_name='amigos+')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'{0}'.format(self.user_id)

    @property
    def nombre_completo(self):
        return '{} {}'.format(self.nombre, self.apellidos)

    @property
    def media_degustaciones(self):
        return self.creador.aggregate(
            media=Avg('calificaciones_degustacion__voto'))['media']

    @property
    def num_degustaciones(self):
        return self.creador.count()

    @property
    def num_calificaciones(self):
        return self.calificaciones_perfil.count()

    @property
    def tiene_actividades(self):
        return self.actividades.filter(vista=False).count() > 0

    def get_absolute_url(self):
        return reverse('usuario-details', args=[str(self.user_id)])

    class Meta:
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfiles'

    def aniadir_amigo(self, perfil, estado=ESTADO_PENDIENTE):
        self.amigos.add(perfil)

    def borrar_amigo(self, perfil, symm=True):
        Amigo.objects.filter(
            de_perfil=self,
            a_perfil=perfil).delete()
        if symm:
            perfil.borrar_amigo(self, False)

    def aceptar_peticion(self, perfil):
        Amigo.objects.filter(
            de_perfil=perfil,
            a_perfil=self).update(estado=ESTADO_ACEPTADA)
        Amigo.objects.create(
            de_perfil=self,
            a_perfil=perfil,
            estado=ESTADO_ACEPTADA)

        self.actividades.create(accion='Has aceptado la peticion de amistad de {}.'.format(
            perfil.nombre), url=perfil.get_absolute_url())

        perfil.actividades.create(accion='{} ha aceptado tu peticion de amistad.'.format(
            self.nombre), url=self.get_absolute_url())

        galardon = Galardon.objects.get(slug='influencer')
        galardon_self = self.galardones_conseguidos.filter(galardon=galardon)
        if self.amigos.count() >= 80 and galardon_self.nivel == 4:
            galardon_self.update(nivel=5)
        elif self.amigos.count() >= 40 and galardon_self.nivel == 3:
            galardon_self.update(nivel=4)
        elif self.amigos.count() >= 20 and galardon_self.nivel == 2:
            galardon_self.update(nivel=3)
        elif self.amigos.count() >= 10 and galardon_self.nivel == 1:
            galardon_self.update(nivel=2)
        elif self.amigos.count() >= 5 and not galardon_self:
            self.galardones_conseguidos.create(galardon=galardon, nivel=1)

        galardon_perfil = perfil.galardones_conseguidos.filter(
            galardon=galardon)
        if perfil.amigos.count() >= 80 and galardon_perfil.nivel == 4:
            galardon_perfil.update(nivel=5)
        elif perfil.amigos.count() >= 40 and galardon_perfil.nivel == 3:
            galardon_perfil.update(nivel=4)
        elif perfil.amigos.count() >= 20 and galardon_perfil.nivel == 2:
            galardon_perfil.update(nivel=3)
        elif perfil.amigos.count() >= 10 and galardon_perfil.nivel == 1:
            galardon_perfil.update(nivel=2)
        elif perfil.amigos.count() >= 5 and not galardon_perfil:
            perfil.galardones_conseguidos.create(galardon=galardon, nivel=1)

    def denegar_peticion(self, perfil):
        Amigo.objects.filter(
            a_perfil=self,
            de_perfil=perfil).update(estado=ESTADO_DENEGADA)

    def bloquear_amigo(self, perfil):
        Amigo.objects.filter(
            de_perfil=self,
            a_perfil=perfil).update(estado=ESTADO_BLOQUEADA)

    def desbloquear_amigo(self, perfil):
        Amigo.objects.filter(
            de_perfil=self,
            a_perfil=perfil,
            estado=ESTADO_BLOQUEADA).update(estado=ESTADO_ACEPTADA)

    def lista_de_amigos(self, estado=None):
        bloqueado = Amigo.objects.filter(
            a_perfil=self, estado=ESTADO_BLOQUEADA).values('de_perfil')

        if estado:
            amigos = Amigo.objects.filter(
                de_perfil=self,
                estado=estado).exclude(a_perfil__in=bloqueado).values_list('a_perfil', flat=True)
            return Perfil.objects.filter(pk__in=amigos)
        else:
            amigos = Amigo.objects.filter(de_perfil=self).exclude(
                a_perfil__in=bloqueado).values_list('a_perfil', flat=True)
            return Perfil.objects.filter(pk__in=amigos)

    @property
    def peticiones_aceptadas(self):
        return self.lista_de_amigos(ESTADO_ACEPTADA)

    @property
    def peticiones_denegadas(self):
        return self.lista_de_amigos(ESTADO_DENEGADA)

    @property
    def peticiones_pendientes(self):
        return self.lista_de_amigos(ESTADO_PENDIENTE)

    @property
    def peticiones_bloqueadas(self):
        return self.lista_de_amigos(ESTADO_BLOQUEADA)

    def lista_peticiones_recividas(self):
        amigos = Amigo.objects.filter(
            a_perfil=self,
            estado=ESTADO_PENDIENTE).values_list('de_perfil', flat=True)

        return Perfil.objects.filter(pk__in=amigos)

    @property
    def peticiones_recibidas(self):
        return self.lista_peticiones_recividas()

    def calificar_degustacion(self, degustacion, voto):
        self.calificaciones_perfil.create(degustacion=degustacion, voto=voto)

    def add_me_gusta_local(self, local):
        self.me_gustas.add(local)

    def delete_me_gusta_local(self, local):
        self.me_gustas.remove(local)


@receiver(post_save, sender=User)
def update_user_perfil(sender, instance, created, **kwargs):
    if created:
        Perfil.objects.create(user=instance)
    instance.perfil.save()


class Amigo(models.Model):
    de_perfil = models.ForeignKey(
        Perfil, on_delete=models.CASCADE, related_name='enviadas')
    a_perfil = models.ForeignKey(
        Perfil, on_delete=models.CASCADE, related_name='recibidas')
    estado = models.IntegerField(
        choices=ESTADO_AMISTAD, default=ESTADO_PENDIENTE)
    # Timestamp
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Amigo'
        verbose_name_plural = 'Amigos'
        unique_together = ['de_perfil', 'a_perfil']


class Local(models.Model):
    creador = models.ForeignKey(
        Perfil, on_delete=models.CASCADE, related_name='creador_local')
    nombre = models.CharField(max_length=50)
    direccion = models.CharField(max_length=200, null=True, blank=True)
    resenia = models.TextField(null=True, blank=True)
    pais = CountryField()
    imagen = models.ImageField(
        upload_to='images/img_local/', default='images/img_local/None/no-img.jpg')
    me_gustas = models.ManyToManyField(
        Perfil, through='MeGusta', related_name='me_gustas')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'{0}'.format(self.nombre)

    def get_absolute_url(self):
        return reverse('local-details', args=[str(self.id)])

    class Meta:
        verbose_name = 'Local'
        verbose_name_plural = 'Locales'


class MeGusta(models.Model):
    perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)
    local = models.ForeignKey(Local, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Me Gusta'
        verbose_name_plural = 'Me Gustas'
        unique_together = ['perfil', 'local']


TIPO_BEBIDA = 0
TIPO_COMIDA = 1
TIPO_TAPA = 2
TIPO_COCTEL = 3
TIPO_RACION = 4
TIPOS = [
    (TIPO_BEBIDA, 'Bebida'),
    (TIPO_COMIDA, 'Comida'),
    (TIPO_TAPA, 'Tapa'),
    (TIPO_COCTEL, 'Coctel'),
    (TIPO_RACION, 'Racion'),
]

TAMANIO_PEQUENIA = 0
TAMANIO_MEDIANA = 1
TAMANIO_GRANDE = 2
TAMANIOS = [
    (TAMANIO_PEQUENIA, 'Pequeña'),
    (TAMANIO_MEDIANA, 'Mediana'),
    (TAMANIO_GRANDE, 'Grande'),
]

GUSTO_DULCE = 0
GUSTO_SALADO = 1
GUSTO_ACIDO = 2
GUSTO_PICANTE = 3
GUSTO_AMARGO = 4
GUSTO_AFRUTADO = 5
GUSTO_UMAMI = 6
GUSTOS = [
    (GUSTO_DULCE, 'Dulce'),
    (GUSTO_SALADO, 'Salado'),
    (GUSTO_ACIDO, 'Acido'),
    (GUSTO_PICANTE, 'Picante'),
    (GUSTO_AMARGO, 'Amargo'),
    (GUSTO_AFRUTADO, 'Afrutado'),
    (GUSTO_UMAMI, 'Umami'),
]


class Degustacion(models.Model):
    creador = models.ForeignKey(
        Perfil, on_delete=models.CASCADE, related_name='creador')
    local = models.ForeignKey(
        Local, on_delete=models.CASCADE, related_name='degustacion_local')
    nombre = models.CharField(max_length=50)
    imagen = models.ImageField(
        upload_to='images/img_degustacion/', default='images/img_degustacion/None/no-img.jpg')
    descripcion = models.TextField()
    tipo = models.IntegerField(choices=TIPOS)
    tamanio = models.IntegerField(choices=TAMANIOS)
    gusto = models.IntegerField(choices=GUSTOS, null=True, blank=True)
    calificaciones = models.ManyToManyField(
        Perfil, through='Calificacion', related_name='calificaciones')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    @property
    def num_calificaciones(self):
        return self.calificaciones.count()

    @property
    def calificacion_media(self):
        return self.calificaciones.aggregate(
            media=Avg('calificaciones__calificaciones_degustacion__voto'))['media']

    def get_absolute_url(self):
        return reverse('degustacion-details', args=[str(self.id)])

    class Meta:
        verbose_name = 'Degustacion'
        verbose_name_plural = 'Degustaciones'


class Calificacion(models.Model):
    perfil = models.ForeignKey(
        Perfil, on_delete=models.CASCADE, related_name='calificaciones_perfil')
    degustacion = models.ForeignKey(
        Degustacion, on_delete=models.CASCADE, related_name='calificaciones_degustacion')
    voto = models.PositiveIntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(5)], null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} - {}'.format(self.perfil.nombre, self.voto)

    class Meta:
        verbose_name = 'Calificacion'
        verbose_name_plural = 'Calificaciones'
        unique_together = ['perfil', 'degustacion']


class Galardon(models.Model):
    slug = models.SlugField()
    nombre = models.CharField(max_length=200)
    icono = models.CharField(max_length=200)
    descripcion = models.TextField()
    perfiles = models.ManyToManyField(
        Perfil, through='GalardonConseguido', related_name='galardones')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Galardon'
        verbose_name_plural = 'Galardones'


NIVELES = [
    (1, 1),
    (2, 2),
    (3, 3),
    (5, 4),
    (5, 5),
]


class GalardonConseguido(models.Model):
    perfil = models.ForeignKey(
        Perfil, on_delete=models.CASCADE, related_name="galardones_conseguidos")
    galardon = models.ForeignKey(Galardon, on_delete=models.CASCADE)
    nivel = models.PositiveIntegerField(choices=NIVELES,
                                        validators=[MinValueValidator(1), MaxValueValidator(5)])

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}-{}-{}'.format(self.perfil, self.galardon, self.nivel)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        galardon_conseguido = super().save(force_insert=force_insert,
                                           force_update=force_update, using=using, update_fields=update_fields)
        self.perfil.actividades.create(accion='Has conseguido el galardon {} de nivel {}.'.format(
            self.galardon.nombre, self.nivel), url='/perfil/galardones')
        return galardon_conseguido

    class Meta:
        verbose_name = 'Galardon conseguido'
        verbose_name_plural = 'Galardones conseguidos'
        unique_together = ['perfil', 'galardon']


class Actividad(models.Model):
    perfil = models.ForeignKey(
        Perfil, on_delete=models.CASCADE, related_name='actividades')
    accion = models.CharField('Accion del usuario', max_length=100)
    url = models.CharField('Url de la actividad', max_length=200)
    vista = models.BooleanField('Accion vista', default=False, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Actividad'
        verbose_name_plural = 'Actividades'
        get_latest_by = ['-created', '-updated']
        ordering = ['-created']


class Comentario(models.Model):
    degustacion = models.ForeignKey(
        Degustacion, on_delete=models.CASCADE, related_name='comentarios_degustacion')
    perfil = models.ForeignKey(
        Perfil, on_delete=models.CASCADE, related_name='comentarios_perfil')
    comentario = models.TextField()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('degustacion-details', args=[str(self.degustacion_id)])

    class Meta:
        verbose_name = 'Comentario'
        verbose_name_plural = 'Comentarios'
        get_latest_by = ['-created', '-updated']
        ordering = ['-created']
